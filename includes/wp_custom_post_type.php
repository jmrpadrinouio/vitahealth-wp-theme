<?php

// Register Custom Post Type
function product() {

    $labels = array(
        'name'                  => _x( 'Products', 'Post Type General Name', 'vitahealth' ),
        'singular_name'         => _x( 'Product', 'Post Type Singular Name', 'vitahealth' ),
        'menu_name'             => __( 'Products', 'vitahealth' ),
        'name_admin_bar'        => __( 'Products', 'vitahealth' ),
        'archives'              => __( 'Products Archive', 'vitahealth' ),
        'attributes'            => __( 'Product Attributes', 'vitahealth' ),
        'parent_item_colon'     => __( 'Parent Product:', 'vitahealth' ),
        'all_items'             => __( 'All Products', 'vitahealth' ),
        'add_new_item'          => __( 'Add New Product', 'vitahealth' ),
        'add_new'               => __( 'Add New', 'vitahealth' ),
        'new_item'              => __( 'New Product', 'vitahealth' ),
        'edit_item'             => __( 'Edit Product', 'vitahealth' ),
        'update_item'           => __( 'Update Product', 'vitahealth' ),
        'view_item'             => __( 'View Product', 'vitahealth' ),
        'view_items'            => __( 'View Products', 'vitahealth' ),
        'search_items'          => __( 'Search Product', 'vitahealth' ),
        'not_found'             => __( 'Not found', 'vitahealth' ),
        'not_found_in_trash'    => __( 'Not found in Trash', 'vitahealth' ),
        'featured_image'        => __( 'Featured Image', 'vitahealth' ),
        'set_featured_image'    => __( 'Set featured image', 'vitahealth' ),
        'remove_featured_image' => __( 'Remove featured image', 'vitahealth' ),
        'use_featured_image'    => __( 'Use as featured image', 'vitahealth' ),
        'insert_into_item'      => __( 'Insert into Product', 'vitahealth' ),
        'uploaded_to_this_item' => __( 'Uploaded to this Product', 'vitahealth' ),
        'items_list'            => __( 'Products list', 'vitahealth' ),
        'items_list_navigation' => __( 'Products list navigation', 'vitahealth' ),
        'filter_items_list'     => __( 'Filter Products list', 'vitahealth' ),
    );
    $args = array(
        'label'                 => __( 'Product', 'vitahealth' ),
        'description'           => __( 'Products of Vitahealth', 'vitahealth' ),
        'labels'                => $labels,
        'supports'              => array( 'title', 'editor', 'thumbnail' ),
        'hierarchical'          => false,
        'public'                => true,
        'show_ui'               => true,
        'show_in_menu'          => true,
        'menu_position'         => 5,
        'menu_icon'             => 'dashicons-sos',
        'show_in_admin_bar'     => true,
        'show_in_nav_menus'     => true,
        'can_export'            => true,
        'has_archive'           => false,
        'exclude_from_search'   => false,
        'publicly_queryable'    => true,
        'capability_type'       => 'post',
    );
    register_post_type( 'product', $args );

}
add_action( 'init', 'product', 0 );
